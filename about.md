---
layout: page
title: About
permalink: /about/
---

```
{
  "name": "Jaxter",
  "location": "Austin, TX, USA",
  "occupation": {
    "title": "Undergraduate Student",
    "location": "University of Texas at Austin"
  }
  "languages": [
    "English",
    "python",
    "Korean",
    "C++",
    "bash"
  ],
  "current_projects": [
    "Learning German",
    "aliqot"
  ],
  "interests": [
    "music technology",
    "hexagons",
    "modularity",
    "command-line interfaces"
    "baking",
    "linux",
    "game design"
  ],
  "free_and_open_source_contributions": [],
}
```
