---
layout: post
title: "on modularity"
permalink: modularity
categories: jekyll thought
---

NOTE: page is still under construction

# Modularity Terminology Proposal

There exist many different types of modular systems, some which benefit more from the features of modularity than others due to the differing design of such systems. Because of these differences, it has become more important to be able to  liguistically distinguish these many types of modularity.

## Types of modularity

### Centralization

A **decentralized** modular system has no central communication/processing unit. Any modules must be at least partially functional on their own.

Examples:

* Swarm robots
* The internet
* Voltron
* A website (with navigation bars)

A **centralized** modular system contains at least one central communication/processing unit without which the system would not function.

Examples:

* A desktop computer
* A car
* A website (with a homepage and no navigation bar)
* The eurorack modular synthesizer system (requires a power module)


### Verticality

A **multi-tiered** modular system is one comprised of modules that are comprised of sub-modules.

Examples:

* A Bitwig device chain
* A large-scale computation system
* The internet

A **single-tiered** modular system comprised of modules that are not themselves modular.

Examples:

* A car
* Voltron

Semantic note: The middle tier of modules are considered both systems and modules.

### Recursiveness

A **self-similar**ly modular system is any multi-tiered modular system where some submodules are architecturally identical to the modules they compose. Generally, these systems can be represented as trees, where root, nodes, and leaves are modules.

Examples:

* A locally executable computer program
* A folder-based filesystem
* A website


A **self-dissimilar**ly modular system is simply defined as a system that is not self-similar.

Example:

* by definition, anything that isn't self-similar


### Modifiability

A **static**ally modular systems are not able to be modified by any party after construction. Though this level of modular design is of no use to the consumer, it often allows the manufacturer to save money when building a product with many different configurations. Often, these modules can be repurposed in a dynamic way, but doing so requires an overhaul of the design of the module, such that it cannot be considered a modular system. These systems are rare, and their opposite can simply be considered the default, though the term **dynamic** could be used when speaking specifically of non-static systems.

Example:

* Cheap MIDI keyboards with various key amount configurations
* A modular home


### User modifiability (subset of modifiability)
A **user-friendly** modular system is intended to be modified at a module level by the user. This definition depends more on the intentions of the manufacturer rather than the physical or technical contstruction of the system. A common sign that a system is not user-modifiable is a voidable warranty. Due to the nature of this definition, there are often systems where some modules are user-modifiable while others are not, smartphones for example.

Examples:

* A normal computer
* A phone with replacable batteries

A **user-hostile** modular system that is only intended to be modified at a module level by the manufacturer or a manufacturer-approved third party. In cases where the manufacturer designs the product in a way that they themselves cannot modify them, the product is considered `statically modular`.

Examples:

* Apple hardware

### Responsiveness (subset of user modifiability)

In a **hot-swapping** modular system, changing out modules does not interrupt the function of the system. common and almost trivial, but not necessary in decentralized systems, as modules are usually removable, but not necessarily insertable. hot-swappable peripherals alone do not make a hot-swappable system. (subset of user-modifiable)

Examples:

* The internet
* swarm robotics
* the european union (apparently)
* the united nations
* A desktop computer system (peripherals)

In a **cold-swapping** modular system, modules can be removed, but not during operation of the system.
Examples:

* A car
* A desktop computer unit

Syntax Note: the terms **hot-swapping** and **cold-swapping** are used to describe systems while the terms **hot-swappable** and **cold-swappable** refer to modules.

### uniform
all modules are the same in their construction.
if there are add-ons that make these modules function differently, but the module is still modularly functional without these add-ons, then the system is still considered uniformly modular. implies, but does not require, an ungendered system.
nonuniform:
modules are distinct in their characteristics

examples:

* swarm robotics

contentions:

* the states in the United States of America could be considered uniform due to their governmental structures and rules given to them by the federal government. However, an important reason for the existence of distinct states is that different people have different interests, so while it is possible for all states to eventually converge to uniformity through governmental overhaul and terraforming, it is antithetical to the philosophical and operational foundations upon which the system exists. to clarify, the state is defined as the bureaucratic structure rather than simply the geography, though the geography does influence the governmental bureaucracy.

### Intercompatibility
any unit can meaningfully connect to any other unit (including similar units)
gendered:
not every unit is compatible with every other unit

examples:

* A computer

counterexamples:

* the internet

partial counterexamples:

* eurorack (there are some units that only have outputs)

## Examples

### Voltron
is a:

* decentralized
* single-tiered
* user-friendly
* cold-swapping
* semi-uniform
* gendered

modular system. It is a rare example of a gendered, uniform modular system.

### GaoGaiGar
is a:

* centralized
* single-tiered
* user-friendly
* cold-swapping
* non-uniform
* gendered

modular system.

### The internet
is a:

* decentralized
* multi-tiered
* self-similar
* user-friendly
* hot-swapping
* uniform
* non-gendered

modular system.

### The European Union
is a:

* semi-decentralized
* single-tiered
* user-unfriendly?
* semi-cold-swapping
* uniform
* non-gendered

modular system.

### Modular synthesizers
### Modular homes

## Glossary

* Peripherals: Auxillary units unique in function or purpose that attach to a central unit
* System: A collection of units that cooperate to perform a task that the individual units could not
* Module: A unit that connects to other units to extend functionality
* Sub-module: A unit that connects to other units to form a module
* Modularity: The state of being composed of modules
