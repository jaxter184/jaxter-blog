---
layout: post
title:  "Terminology Reference"
permalink: terms
categories: jekyll thought
---

This page is a list of the terms that I like to use and why I prefer them over their more commonly used counterparts.

### free-source
The Free Software Movement makes a good point that the term 'open-source' emphasizes a distinct connotation from the term 'free'. However, though the term 'free software' is intuitively understood within this community, when speaking to an outsider or when an outsider uses the term, there is ambiguity that must be clarified every time, just in case there is confusion. On the other hand, the term 'free-source' is not only distinct from 'open-source', but also from 'free'. The speaker can avoid ambiguity by using the adjective 'free' to refer to the source code of the software in question rather than the software itself, as source code almost never costs money, and is more often either free or restricted, rather than free or paid. If the outsider does not know anything about the Free Software movement, the term 'free-source' raises a flag and prompts a question, while the term 'free' is easily mistaken for "free of charge". An added bonus is that FOSS can refer to (free/open)-source software, as in software that is both free-source and open-source.

### leader/minion
The term master/slave not only has negative social connotations, but also is an inaccurate term to describe the relationship between most of the devices that are labelled with the term. As I am underqualified to talk about the social ramifications, my reasoning for using the terms 'leader/minion' are primarily due to the more intuitive understanding that one has from the connotations of those terms. Unlike in a master/slave relationship, a leader their minions work together to accomplish something that they could not accomplish alone. While a minion works at the command of the leader, they both have the same goal, and communicate information bidirectionally. While I would like to use the terms 'master/minion' to allow listeners to be able to use context to know that I am talking about what is traditionally referred to as 'master/slave', because they start with the same letter, they cannot be shortened in the way that the other terms can be shortened to 'MISO/MOSI' and 'LIMO/LOMI'. Also I really like the term 'minion'.

### pianjo
An alternative to keytar. Pretty self-explanatory, I think.
