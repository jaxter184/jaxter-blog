---
layout: post
title:  "on trackballs"
date:   2019-10-18 15:06:40 -0500
permalink: trackball
categories: jekyll thought input
---

## Pros:
+ Very very space efficient
+ Related to the previous, but it can be used on any surface. I often place the trackball on my leg or on the windowsill of the bus. Even my massive trackball that's not intended for travel is easier to travel with than the most portable mice I've seen due to the freedom of placement.
+ Less arm movement. I use Dvorak for the same reason.
+ More thumb usage. It's the strongest finger, why don't we use it more?

## Cons:
- The muscles that you have to use to move the cursor a given amount in a given direction change depending on the current hand and finger position due to the spherical nature of the ball. Mice do not have this problem because they are usually used on a flat surface. Imagine using a mouse on a spherical surface.
- Less selection

## Recommendations:

### [Elecom HUGE](https://www.elecom.co.jp.e.gj.hp.transer.com/products/M-HT1DRBK.html)

Best large trackball I have used. Better than the Kensington Expert in ergonomics, function buttons, and scroll mechanics. As much as I love scroll rings, they really aren't easy to use. Comes in both wired and wireless USB dongle varieties.

### [Elecom DEFT PRO](https://www.elecom.co.jp.e.gj.hp.transer.com/products/M-DPT1MRXBK.html)

This is the trackball I use with my laptop. The Bluetooth option is a huge workflow improvement, but my favorite thing about it is that it allows for wired USB, wireless USB or Bluetooth in the same model, as opposed to the Elecom HUGE, which has two separate models for wired and wireless, and no Bluetooth on either. Also has one fewer function key than the HUGE.

### [Logitech M570](https://www.logitech.com/en-us/product/wireless-trackball-m570)

A good entry level trackball. I personally don't like thumb balls, but all of my friends seem to prefer them over finger balls, possibly due to the positioning of the left and right click. Finding left click seems to be the first issue that people have when they try to use my trackballs. I personally think it only takes about 30 seconds to get used to the positioning on a finger ball, but mileage may vary.

### [Logitech MX Ergo](https://www.logitech.com/en-us/product/mx-ergo-wireless-trackball-mouse)

I have never owned or used one of these, so I can't officially endorse it, but it seems to be the thumb ball equivalent of the Elecom DEFT PRO. Very pricey though.
