---
layout: post
title:  "Website"
permalink: website
categories: jekyll project software web
---

## Why make a website?

The way the internet is structured makes it very easy for an individual to broadcast their voice to others. Most people use social media for this type of thing, but most of my favorite places to read about what people are up to are blogs. The straightforward, yet flexible structure of a blog post seems to be to be the most clear and easy way to express ideas over the internet. Though videos and podcasts are also a great way to convey information, they take much more time and energy to produce than a blog post.

## How make a website?

One of the first "programming languages" I learned was HTML. I remember being a middle schooler (or maybe an elementary schooler? I don't really remember) thinking it was so cool to be able to write a website with ordered and unordered lists, and headers, and music. If only the internet were so simple. My first actual website was published in 2016 or so, and was built from scratch in HTML with little to no CSS. It was super simple and the most interesting bit, the menu navigation, was mostly copy/pasted from a codepen example, and didn't even work properly.

This iteration of the website is made using Jekyll, a static website generator. I basically pick a theme, write some markdown files, and boom, website. I had some issues getting the GitLab pages pipeline to work properly, and some redirection authentication shenanigans, but overall, this has been a much smoother and simpler process. I like this a little more because it lets me focus on the actual content of the site rather than the minute details of how everything is going to look. Going forward, I'm definitely going to write my own Jekyll theme, once I figure out how.
